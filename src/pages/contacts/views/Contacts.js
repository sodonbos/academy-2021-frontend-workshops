import React from 'react'
// components
import ContactList from '../components/ContactList'
import ContactDetail from '../components/ContactDetail'
import Contact from '../components/Contact'

function Contacts() {

  return (
    <div className="container">
        <ContactList>
            <Contact name="Jakub Kočiščák" color="yellowgreen" isActive/>
            <Contact name="Katka D." color="green"/>
            <Contact name="Janko Kočiščák" color="red" />
        </ContactList>

        <ContactDetail name="Janko Kočiščák" phone="+421 903 399 833" color="red" />
    </div>
  );
}

export default Contacts;
