import React from 'react';
import Avatar from '../../../components/Avatar';
import Button from '../../../components/Button';

const ContactDetail = ( { name, phone, color } ) => {
    return (
        <div className="contact-detail">
            <div className="center-flex m-5">
                <Avatar letter={ name.charAt(0) } scale="3" style={{ background: color }} />
            </div>
            <h1> { name } </h1>
            <h3> { phone } </h3>

            <div className="center-flex mr-4 ml-4">
                <Button text="Make a call" style={{ background: color }} />
                <Button text="Send SMS" style={{ background: "green" }} />
            </div>
        </div>
    );
};

export default ContactDetail;