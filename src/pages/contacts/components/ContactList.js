import React from 'react';
// components
import Button from '../../../components/Button';

const ContactList = ( { children } ) => {

    return (
        <div className="contact-list">
            <h2 style={{ padding: "1rem" }}> Contact list </h2>
            
            { children 
                ? children 
                : <span class="center-flex"> No contacts </span> 
            }

            <Button text="Add contact"/>            
        </div>
    );
};

export default ContactList;