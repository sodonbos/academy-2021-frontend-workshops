import React from 'react';
// components
import Avatar from '../../../components/Avatar';

const Contact = ( { name, color, isActive } ) => {
    return (
        <div className={ isActive ? "avatar-group active" : "avatar-group" }>
            <Avatar letter={ name.charAt(0) } style={ { background: color } }/>
            <h3 className="avatar-name"> { name } </h3>
        </div>
    );
};

export default Contact;