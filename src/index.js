import React from 'react';
import ReactDOM from 'react-dom';
import Contacts from './pages/contacts/views/Contacts';

import './styles/styles.css'

ReactDOM.render(
    <Contacts />,
    document.getElementById('root')
);