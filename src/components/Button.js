import React from 'react';

const Button = ( { text, style } ) => {
    return (
        <button className="button" style={ style }>
            { text }
        </button>
    );
};

export default Button;