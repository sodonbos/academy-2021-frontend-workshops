import React from 'react';

const Avatar = ( { letter, style, scale } ) => {

    return (
        <div className="avatar" style={ scale ? {
            ...style,
            scale: scale
        } : style }>
            { letter } 
        </div>
    );
};

export default Avatar;